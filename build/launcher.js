!function(modules) {
    var installedModules = {};
    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) return installedModules[moduleId].exports;
        var module = installedModules[moduleId] = {
            i: moduleId,
            l: !1,
            exports: {}
        };
        return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__), 
        module.l = !0, module.exports;
    }
    __webpack_require__.m = modules, __webpack_require__.c = installedModules, __webpack_require__.d = function(exports, name, getter) {
        __webpack_require__.o(exports, name) || Object.defineProperty(exports, name, {
            enumerable: !0,
            get: getter
        });
    }, __webpack_require__.r = function(exports) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(exports, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(exports, "__esModule", {
            value: !0
        });
    }, __webpack_require__.t = function(value, mode) {
        if (1 & mode && (value = __webpack_require__(value)), 8 & mode) return value;
        if (4 & mode && "object" == typeof value && value && value.__esModule) return value;
        var ns = Object.create(null);
        if (__webpack_require__.r(ns), Object.defineProperty(ns, "default", {
            enumerable: !0,
            value
        }), 2 & mode && "string" != typeof value) for (var key in value) __webpack_require__.d(ns, key, function(key) {
            return value[key];
        }.bind(null, key));
        return ns;
    }, __webpack_require__.n = function(module) {
        var getter = module && module.__esModule ? function getDefault() {
            return module.default;
        } : function getModuleExports() {
            return module;
        };
        return __webpack_require__.d(getter, "a", getter), getter;
    }, __webpack_require__.o = function(object, property) {
        return Object.prototype.hasOwnProperty.call(object, property);
    }, __webpack_require__.p = "", __webpack_require__(__webpack_require__.s = 93);
}({
    11: function(module, exports) {
        module.exports = require("path");
    },
    2: function(module, exports) {
        module.exports = require("@taraflex/string-tools");
    },
    21: function(module, exports) {
        module.exports = require("hogan.js");
    },
    22: function(module, exports) {
        module.exports = require("convict");
    },
    3: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_require__.d(__webpack_exports__, "c", (function() {
            return CONFIG_LOCATION;
        })), __webpack_require__.d(__webpack_exports__, "j", (function() {
            return getDef;
        })), __webpack_require__.d(__webpack_exports__, "i", (function() {
            return config;
        })), __webpack_require__.d(__webpack_exports__, "f", (function() {
            return MEMORY;
        })), __webpack_require__.d(__webpack_exports__, "g", (function() {
            return PORT;
        })), __webpack_require__.d(__webpack_exports__, "e", (function() {
            return IP;
        })), __webpack_require__.d(__webpack_exports__, "a", (function() {
            return ADMIN;
        })), __webpack_require__.d(__webpack_exports__, "h", (function() {
            return STATIC;
        })), __webpack_require__.d(__webpack_exports__, "b", (function() {
            return API;
        })), __webpack_require__.d(__webpack_exports__, "d", (function() {
            return HEALTH;
        }));
        var convict__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(22), convict__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(convict__WEBPACK_IMPORTED_MODULE_0__);
        const CONFIG_LOCATION = __dirname + "/config.json", convict = convict__WEBPACK_IMPORTED_MODULE_0___default()({
            ip: {
                doc: "The IP address to bind.",
                format: "ipaddress",
                default: "0.0.0.0",
                env: "REGULUS_IP",
                arg: "ip"
            },
            port: {
                doc: "The port to bind.",
                format: "port",
                default: 8080,
                env: "REGULUS_PORT",
                arg: "port"
            },
            memory: {
                doc: "Maximum allowed memory size for nodejs",
                format: "nat",
                default: 512,
                env: "REGULUS_MEMORY",
                arg: "memory"
            }
        });
        try {
            convict.loadFile(CONFIG_LOCATION);
        } catch (err) {
            err && "ENOENT" == err.code || console.error("(core/utils/config.ts:31)", err);
        }
        function getDef(prop) {
            return convict.default(prop);
        }
        convict.validate();
        const config = Object.freeze(convict.getProperties()), MEMORY = convict.get("memory"), PORT = convict.get("port"), IP = convict.get("ip"), ADMIN = "/adm", STATIC = "/static", API = "/api", HEALTH = "/health";
    },
    36: function(module, exports) {
        module.exports = require("json5");
    },
    5: function(module, exports) {
        module.exports = require("fs");
    },
    71: function(module, exports) {
        module.exports = require("child-process-promise");
    },
    72: function(module, exports) {
        module.exports = require("fast-deep-equal");
    },
    73: function(module, exports) {
        module.exports = require("last-match");
    },
    74: function(module, exports) {
        module.exports = require("random-number");
    },
    93: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_require__.r(__webpack_exports__);
        var child_process_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(71), fast_deep_equal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(72), fast_deep_equal__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(fast_deep_equal__WEBPACK_IMPORTED_MODULE_1__), fs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5), json5__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(36), json5__WEBPACK_IMPORTED_MODULE_3___default = __webpack_require__.n(json5__WEBPACK_IMPORTED_MODULE_3__), last_match__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(73), last_match__WEBPACK_IMPORTED_MODULE_4___default = __webpack_require__.n(last_match__WEBPACK_IMPORTED_MODULE_4__), path__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(11), random_number__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(74), random_number__WEBPACK_IMPORTED_MODULE_6___default = __webpack_require__.n(random_number__WEBPACK_IMPORTED_MODULE_6__), _taraflex_string_tools__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2), _utils_config__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3);
        const {argv} = __webpack_require__(94), nginxTpl = __webpack_require__(95), proxyTpl = __webpack_require__(96);
        function readConfig(filename) {
            try {
                return json5__WEBPACK_IMPORTED_MODULE_3___default.a.parse(Object(fs__WEBPACK_IMPORTED_MODULE_2__.readFileSync)(filename, "utf-8"));
            } catch {}
            return {};
        }
        function readConfigPort(filename) {
            try {
                return 0 | readConfig(filename).port;
            } catch {}
            return 0;
        }
        async function e(cmd) {
            const {stdout, stderr} = await Object(child_process_promise__WEBPACK_IMPORTED_MODULE_0__.exec)(cmd), t = stdout || stderr;
            return Object(_taraflex_string_tools__WEBPACK_IMPORTED_MODULE_7__.trim)(t && t.toString());
        }
        async function ec(cmd) {
            try {
                return await e(cmd);
            } catch (e) {
                const t = e.stderr || e.stdout;
                return Object(_taraflex_string_tools__WEBPACK_IMPORTED_MODULE_7__.trim)(t && t.toString());
            }
        }
        async function findFreePort(blackListPorts, port) {
            for (;blackListPorts.has(port) || await ec("lsof -i :" + port) || await ec("lsof -i :" + (port + 1)); ) blackListPorts.add(port), 
            port = random_number__WEBPACK_IMPORTED_MODULE_6___default()({
                min: 3e3,
                max: 49150,
                integer: !0
            });
            return blackListPorts.add(port), blackListPorts.add(port + 1), port;
        }
        (async function main() {
            const {hostname} = argv;
            if (!hostname) throw "Hostname required";
            const NGINX_CONF_PATH = "/etc/nginx/sites-available/" + hostname;
            await ec(`pm2 delete regulus-admin-${_utils_config__WEBPACK_IMPORTED_MODULE_8__.g} && tmux kill-session -t regulus-admin-${_utils_config__WEBPACK_IMPORTED_MODULE_8__.g}`);
            const pmInfo = json5__WEBPACK_IMPORTED_MODULE_3___default.a.parse(await e("pm2 prettylist")).map(v => v.pm2_env.pm_exec_path).filter(Boolean), blackListPorts = new Set(pmInfo.map(v => readConfigPort(v + "/../config.json")).filter(Boolean).concat(Object(_utils_config__WEBPACK_IMPORTED_MODULE_8__.j)("port"), 0, 1, 1e4, 10001, 10009, 10010, 10024, 10025, 10042, 10050, 10051, 10080, 101, 1010, 1011, 10110, 10172, 102, 1020, 10200, 10201, 10204, 10212, 1023, 1024, 1027, 1028, 1029, 10308, 104, 10480, 105, 10505, 10514, 1058, 1059, 107, 108, 1080, 10823, 1085, 10891, 109, 10933, 1098, 1099, 11, 110, 11001, 1109, 111, 11111, 11112, 1119, 11211, 11214, 11215, 11235, 113, 11311, 11371, 115, 1167, 117, 11753, 118, 119, 1194, 1198, 12012, 12013, 12035, 12043, 12046, 1214, 1220, 12201, 12222, 12223, 123, 1234, 12345, 1241, 12443, 12489, 126, 1270, 1293, 12975, 13, 13e3, 13008, 13050, 13075, 1311, 1314, 1337, 1341, 1344, 135, 1352, 1360, 137, 13720, 13721, 13724, 13782, 13783, 13785, 13786, 138, 139, 1414, 1417, 1418, 1419, 1420, 143, 1431, 1433, 1434, 14550, 14567, 1492, 1494, 15, 1500, 15e3, 1501, 1503, 1512, 1513, 152, 1521, 1524, 1527, 153, 1533, 15345, 1540, 1541, 1542, 15441, 1545, 1547, 1550, 15567, 156, 1560, 15672, 158, 1581, 1582, 1583, 1589, 1591, 16e3, 1604, 16080, 161, 162, 16200, 16225, 16250, 1626, 16261, 1627, 1628, 1629, 16300, 16384, 16387, 16393, 16400, 16402, 16403, 1645, 1646, 16472, 16482, 16567, 1666, 1677, 1688, 17, 170, 1701, 17011, 1707, 1716, 1719, 1720, 1723, 17500, 1755, 1761, 177, 1783, 179, 18, 1801, 18091, 18092, 18104, 1812, 1813, 18200, 18201, 18206, 18300, 18301, 18306, 18333, 18400, 18401, 18505, 18506, 18605, 18606, 1863, 1880, 1883, 19, 1900, 19e3, 19001, 19132, 19150, 19226, 19294, 19295, 19302, 1935, 194, 1967, 1970, 1972, 19812, 19813, 19814, 1984, 1985, 199, 1998, 19999, 20, 2e3, 2e4, 201, 2010, 2033, 2049, 2056, 20560, 20595, 2080, 20808, 2082, 2083, 2086, 2087, 209, 2095, 2096, 21, 210, 2100, 2101, 2102, 21025, 2103, 2104, 2123, 213, 2142, 2152, 2159, 218, 2181, 2195, 2196, 22, 220, 22e3, 2210, 2211, 22136, 2221, 2222, 22222, 2226, 225, 2261, 2262, 2266, 23, 2302, 2303, 2305, 23073, 23399, 2351, 23513, 2368, 2369, 2370, 2372, 2375, 2376, 2377, 2379, 2380, 2399, 2401, 2404, 241, 2424, 2427, 24441, 24444, 24465, 2447, 24554, 2480, 24800, 2483, 2484, 24842, 249, 25, 2535, 2541, 2546, 2548, 255, 25565, 25575, 25826, 259, 2593, 2598, 2599, 26e3, 262, 2638, 264, 26900, 26901, 27e3, 27006, 27009, 27015, 27016, 27017, 27018, 27030, 27031, 27036, 27037, 2710, 2727, 27374, 27500, 27888, 27900, 27901, 27910, 27950, 27960, 27969, 280, 28001, 28015, 2809, 2811, 2827, 28770, 28771, 28785, 28786, 28852, 28910, 28960, 29e3, 29070, 2944, 2945, 2947, 2948, 2949, 2967, 29900, 29901, 29920, 300, 3e3, 3004, 3020, 3050, 3052, 30564, 3074, 308, 3101, 311, 3128, 31337, 31416, 31438, 31457, 318, 319, 320, 32137, 3225, 3233, 32400, 3260, 3268, 3269, 32764, 3283, 32887, 3290, 32976, 3305, 3306, 3313, 3316, 3323, 3332, 3333, 33434, 3351, 33848, 3386, 3389, 3396, 34e3, 3412, 34197, 3423, 3424, 3455, 3478, 3479, 3480, 3483, 3493, 350, 351, 3516, 3527, 3535, 35357, 3544, 356, 3632, 3645, 3659, 366, 3667, 3689, 369, 3690, 37, 370, 37008, 3702, 371, 3724, 3725, 3768, 3784, 3785, 3799, 38, 3804, 3825, 3826, 383, 3830, 3835, 384, 3856, 3868, 387, 3872, 3880, 389, 39, 3900, 3960, 3962, 3978, 3979, 399, 3999, 4e3, 4e4, 4001, 401, 4018, 4035, 4045, 4050, 4069, 4089, 4090, 4093, 4096, 4105, 4111, 4116, 4125, 4172, 4190, 4198, 42, 4201, 4222, 4226, 4242, 4243, 4244, 427, 43, 4303, 4307, 43110, 4321, 433, 434, 4352, 43594, 43595, 443, 444, 44405, 4444, 4445, 445, 44818, 4486, 4488, 4500, 4502, 4505, 4506, 4534, 4560, 4567, 4569, 4604, 4605, 4610, 464, 4640, 465, 4662, 4664, 4672, 47, 47001, 4711, 4713, 4728, 4730, 4739, 4747, 475, 4750, 4753, 47808, 4789, 4840, 4843, 4847, 4848, 4894, 49, 491, 49151, 4949, 4950, 497, 5, 50, 500, 5e3, 5001, 5002, 5003, 5004, 5005, 5010, 5011, 502, 5025, 5031, 5037, 504, 5048, 5050, 5051, 5060, 5061, 5062, 5064, 5065, 5070, 5084, 5085, 5093, 5099, 51, 510, 5104, 512, 5121, 5124, 5125, 513, 514, 515, 5150, 5151, 5154, 517, 5172, 518, 5190, 5198, 5199, 52, 520, 5200, 5201, 521, 5222, 5223, 5228, 524, 5242, 5243, 5246, 5247, 525, 5269, 5280, 5281, 5298, 53, 530, 5310, 5318, 532, 533, 5349, 5351, 5353, 5355, 5357, 5358, 5394, 54, 540, 5402, 5405, 5412, 5413, 5417, 542, 5421, 543, 5432, 5433, 544, 5445, 546, 547, 548, 5480, 5481, 5495, 5498, 5499, 550, 5500, 5501, 5517, 554, 5550, 5554, 5555, 5556, 556, 5568, 56, 560, 5601, 561, 563, 5631, 5632, 564, 5656, 5666, 5667, 5670, 5671, 5672, 5678, 5683, 57, 5701, 5718, 5719, 5722, 5723, 5724, 5741, 5742, 58, 5800, 585, 587, 5900, 591, 593, 5931, 5938, 5984, 5985, 5986, 5988, 5989, 6e3, 6005, 6009, 601, 604, 6050, 6051, 6063, 6086, 61, 6100, 6101, 6110, 6111, 6112, 6113, 6136, 6159, 6200, 6201, 6225, 6227, 623, 6240, 6244, 625, 6255, 6257, 6260, 6262, 631, 6343, 6346, 6347, 635, 6350, 636, 6379, 6389, 639, 641, 643, 6432, 6436, 6437, 6444, 6445, 646, 6463, 6464, 647, 6472, 648, 6502, 651, 6513, 6514, 6515, 653, 654, 6543, 655, 6556, 6560, 6561, 6566, 657, 6571, 660, 6600, 6601, 6602, 6619, 6622, 6653, 666, 6660, 6664, 6665, 6669, 6679, 6690, 6697, 6699, 67, 6715, 674, 6771, 6783, 6785, 6789, 68, 6869, 688, 6881, 6887, 6888, 6889, 6890, 6891, 69, 690, 6900, 6901, 6902, 691, 694, 695, 6968, 6969, 6970, 698, 6999, 7, 70, 700, 7e3, 7001, 7002, 7005, 7006, 701, 7010, 702, 7022, 7023, 7025, 7047, 706, 7070, 71, 711, 712, 7133, 7144, 7145, 7171, 7262, 7272, 7306, 7307, 7312, 7396, 74, 7400, 7401, 7402, 7471, 7473, 7474, 7478, 749, 75, 750, 751, 752, 753, 754, 7542, 7547, 7575, 760, 7624, 7631, 7634, 7652, 7654, 7655, 7656, 7660, 7670, 7687, 77, 7707, 7708, 7717, 7777, 7788, 782, 783, 7831, 7880, 7890, 79, 7915, 7935, 7946, 7990, 80, 800, 8e3, 8005, 8006, 8007, 8008, 8009, 8042, 8069, 8070, 8074, 8075, 808, 8080, 8088, 8089, 8090, 8091, 8092, 81, 8111, 8112, 8116, 8118, 8123, 8139, 8140, 8172, 8184, 8194, 8195, 82, 8200, 8222, 8243, 8245, 8280, 8281, 829, 8291, 830, 8303, 831, 832, 833, 8332, 8333, 8337, 8384, 8388, 843, 8443, 8444, 847, 848, 8484, 8500, 853, 8530, 8531, 8580, 860, 861, 862, 8629, 8642, 8691, 87, 873, 8767, 88, 8834, 8840, 888, 8880, 8883, 8887, 8888, 8889, 897, 898, 8983, 8997, 8998, 8999, 9, 90, 9e3, 9001, 9002, 9006, 902, 903, 9030, 9042, 9043, 9050, 9051, 9060, 9080, 9090, 9091, 9092, 9100, 9101, 9102, 9103, 9119, 9150, 9191, 9199, 9200, 9217, 9293, 9300, 9303, 9306, 9309, 9312, 9332, 9333, 9339, 9389, 9418, 9419, 9420, 9421, 9422, 9425, 9443, 953, 9535, 9536, 9600, 9675, 9676, 9695, 9785, 9800, 981, 987, 9875, 989, 9898, 9899, 99, 990, 991, 992, 993, 994, 995, 9981, 9982, 9987, 9993, 9997, 9999)), appPort = await findFreePort(blackListPorts, _utils_config__WEBPACK_IMPORTED_MODULE_8__.g), appName = "regulus-admin-" + appPort;
            !function updateConfig(port) {
                const oldConfig = readConfig(_utils_config__WEBPACK_IMPORTED_MODULE_8__.c);
                for (let p in oldConfig) oldConfig[p] === Object(_utils_config__WEBPACK_IMPORTED_MODULE_8__.j)(p) && delete oldConfig[p];
                const newConfig = {
                    ..._utils_config__WEBPACK_IMPORTED_MODULE_8__.i,
                    port
                };
                for (let p in newConfig) newConfig[p] === Object(_utils_config__WEBPACK_IMPORTED_MODULE_8__.j)(p) && delete newConfig[p];
                fast_deep_equal__WEBPACK_IMPORTED_MODULE_1___default()(oldConfig, newConfig) || Object(fs__WEBPACK_IMPORTED_MODULE_2__.writeFileSync)(_utils_config__WEBPACK_IMPORTED_MODULE_8__.c, JSON.stringify(newConfig, null, "  "));
            }(appPort);
            let proxyPort = +last_match__WEBPACK_IMPORTED_MODULE_4___default()(Object(fs__WEBPACK_IMPORTED_MODULE_2__.readFileSync)("/etc/tinyproxy/tinyproxy.conf", "utf-8"), /^Port\s+(\d+)/m) || await findFreePort(blackListPorts, _utils_config__WEBPACK_IMPORTED_MODULE_8__.g + 2);
            const apps = pmInfo.map(v => {
                const port = readConfigPort(v + "/../config.json");
                return {
                    port,
                    devport: port + 1,
                    staticDir: Object(path__WEBPACK_IMPORTED_MODULE_5__.resolve)(v + "/../static")
                };
            });
            apps.some(v => v.port == appPort) || apps.push({
                port: appPort,
                devport: appPort + 1,
                staticDir: Object(path__WEBPACK_IMPORTED_MODULE_5__.resolve)(__dirname + "/static")
            });
            const hosts = `-d ${hostname} ` + apps.map(a => `-d ${a.port}.${hostname} -d dev-${a.port}.${hostname}`).join(" ");
            Object(fs__WEBPACK_IMPORTED_MODULE_2__.writeFileSync)(NGINX_CONF_PATH, nginxTpl({
                apps,
                hostname
            })), Object(fs__WEBPACK_IMPORTED_MODULE_2__.writeFileSync)("/etc/tinyproxy/tinyproxy.conf", proxyTpl({
                port: proxyPort
            })), await Promise.all([ ec("tmux kill-session -t " + appName), ec("pm2 delete " + appName), ec("pm2 startup"), ec("/root/.acme.sh/acme.sh --issue --nginx " + hosts), ec("systemctl restart tinyproxy") ]), 
            console.log(await e(`/root/.acme.sh/acme.sh --install-cert --force ${hosts} --key-file /etc/nginx/ssl/key.pem --fullchain-file /etc/nginx/ssl/cert.pem --reloadcmd "systemctl reload nginx"`)), 
            console.log(await e(`pm2 start "${__dirname}/index.js" --name ${appName} --node-args="--max_old_space_size=${_utils_config__WEBPACK_IMPORTED_MODULE_8__.f}" --log-date-format 'DD.MM HH:mm:ss'`)), 
            process.exit(0);
        })().catch(err => console.error("(src/core/launcher.ts:132)", err));
    },
    94: function(module, exports) {
        module.exports = require("yargs");
    },
    95: function(module, exports, __webpack_require__) {
        var H = __webpack_require__(21);
        module.exports = function() {
            var T = new H.Template({
                code: function(c, p, i) {
                    var t = this;
                    return t.b(i = i || ""), t.b("server_tokens off;"), t.b("\n" + i), t.b("autoindex off;"), 
                    t.b("\n"), t.b("\n" + i), t.b("access_log /var/log/nginx/access.log;"), t.b("\n" + i), 
                    t.b("error_log /var/log/nginx/error.log;"), t.b("\n"), t.b("\n" + i), t.b("map $http_upgrade $connection_upgrade {"), 
                    t.b("\n" + i), t.b("    default upgrade;"), t.b("\n" + i), t.b("    ''      close;"), 
                    t.b("\n" + i), t.b("}"), t.b("\n"), t.b("\n" + i), t.b("map $request_uri $request_path {"), 
                    t.b("\n" + i), t.b('    "~^(?<path>[^?]*)" $path;'), t.b("\n" + i), t.b("}"), t.b("\n"), 
                    t.b("\n" + i), t.b("map $request_path $last_request_path {"), t.b("\n" + i), t.b('    "~(?<path>[^/]*)$" $path;'), 
                    t.b("\n" + i), t.b("}"), t.b("\n"), t.b("\n" + i), t.s(t.f("apps", c, p, 1), c, p, 0, 340, 3866, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                        t.b("\n" + i), t.b("upstream tunnel"), t.b(t.t(t.f("devport", c, p, 0))), t.b(" {"), 
                        t.b("\n" + i), t.b("    server 127.0.0.1:"), t.b(t.t(t.f("devport", c, p, 0))), 
                        t.b(";"), t.b("\n" + i), t.b("}"), t.b("\n"), t.b("\n" + i), t.b("server {"), t.b("\n" + i), 
                        t.b("    listen 80;"), t.b("\n" + i), t.b("    listen [::]:80;"), t.b("\n" + i), 
                        t.b("    server_name dev-"), t.b(t.t(t.f("port", c, p, 0))), t.b("."), t.b(t.t(t.f("hostname", c, p, 0))), 
                        t.b(";"), t.b("\n" + i), t.b("    return 301 https://dev-"), t.b(t.t(t.f("port", c, p, 0))), 
                        t.b("."), t.b(t.t(t.f("hostname", c, p, 0))), t.b("$request_uri;"), t.b("\n" + i), 
                        t.b("}"), t.b("\n"), t.b("\n" + i), t.b("server {    "), t.b("\n" + i), t.b("    listen 443 ssl;"), 
                        t.b("\n" + i), t.b("    listen [::]:443 ssl;"), t.b("\n" + i), t.b("    "), t.b("\n" + i), 
                        t.b("    ssl_certificate /etc/nginx/ssl/cert.pem;"), t.b("\n" + i), t.b("    ssl_certificate_key /etc/nginx/ssl/key.pem;"), 
                        t.b("\n" + i), t.b("    ssl_protocols TLSv1.2;"), t.b("\n" + i), t.b("      "), 
                        t.b("\n" + i), t.b("    root "), t.b(t.t(t.f("staticDir", c, p, 0))), t.b(";"), 
                        t.b("\n" + i), t.b("    server_name dev-"), t.b(t.t(t.f("port", c, p, 0))), t.b("."), 
                        t.b(t.t(t.f("hostname", c, p, 0))), t.b(";"), t.b("\n"), t.b("\n" + i), t.b("    gzip off;"), 
                        t.b("\n"), t.b("\n" + i), t.b("    ssl_certificate /etc/nginx/ssl/cert.pem;"), t.b("\n" + i), 
                        t.b("    ssl_certificate_key /etc/nginx/ssl/key.pem;"), t.b("\n"), t.b("\n" + i), 
                        t.b("    location / {"), t.b("\n" + i), t.b('        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;'), 
                        t.b("\n" + i), t.b('        add_header X-Frame-Options "DENY" always;'), t.b("\n" + i), 
                        t.b('        add_header X-Content-Type-Options "nosniff" always;'), t.b("\n" + i), 
                        t.b('        add_header X-Permitted-Cross-Domain-Policies "none" always;'), t.b("\n" + i), 
                        t.b('        add_header X-XSS-Protection "1; mode=block" always;'), t.b("\n"), t.b("\n" + i), 
                        t.b("        proxy_pass http://tunnel"), t.b(t.t(t.f("devport", c, p, 0))), t.b(";"), 
                        t.b("\n" + i), t.b("        proxy_http_version 1.1;"), t.b("\n" + i), t.b("        proxy_set_header Host $http_host;"), 
                        t.b("\n" + i), t.b("        proxy_set_header X-Real-IP $remote_addr;"), t.b("\n" + i), 
                        t.b("        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"), t.b("\n" + i), 
                        t.b("        proxy_set_header X-Forwarded-Proto $scheme;"), t.b("\n" + i), t.b("    }"), 
                        t.b("\n" + i), t.b("}"), t.b("\n"), t.b("\n" + i), t.b("server {"), t.b("\n" + i), 
                        t.b("    listen 80;"), t.b("\n" + i), t.b("    listen [::]:80;"), t.b("\n" + i), 
                        t.b("    server_name "), t.b(t.t(t.f("port", c, p, 0))), t.b("."), t.b(t.t(t.f("hostname", c, p, 0))), 
                        t.b(";"), t.b("\n" + i), t.b("    return 301 https://"), t.b(t.t(t.f("port", c, p, 0))), 
                        t.b("."), t.b(t.t(t.f("hostname", c, p, 0))), t.b("$request_uri;"), t.b("\n" + i), 
                        t.b("}"), t.b("\n"), t.b("\n" + i), t.b("server {    "), t.b("\n" + i), t.b("    listen 443 ssl;"), 
                        t.b("\n" + i), t.b("    listen [::]:443 ssl;"), t.b("\n" + i), t.b("    "), t.b("\n" + i), 
                        t.b("    ssl_certificate /etc/nginx/ssl/cert.pem;"), t.b("\n" + i), t.b("    ssl_certificate_key /etc/nginx/ssl/key.pem;"), 
                        t.b("\n" + i), t.b("    ssl_protocols TLSv1.2;"), t.b("\n" + i), t.b("      "), 
                        t.b("\n" + i), t.b("    root "), t.b(t.t(t.f("staticDir", c, p, 0))), t.b(";"), 
                        t.b("\n" + i), t.b("    server_name "), t.b(t.t(t.f("port", c, p, 0))), t.b("."), 
                        t.b(t.t(t.f("hostname", c, p, 0))), t.b(";"), t.b("\n"), t.b("\n" + i), t.b("    gzip off;"), 
                        t.b("\n"), t.b("\n" + i), t.b("    ssl_certificate /etc/nginx/ssl/cert.pem;"), t.b("\n" + i), 
                        t.b("    ssl_certificate_key /etc/nginx/ssl/key.pem;"), t.b("\n"), t.b("\n" + i), 
                        t.b("    location / {"), t.b("\n" + i), t.b('        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;'), 
                        t.b("\n" + i), t.b('        add_header X-Frame-Options "DENY" always;'), t.b("\n" + i), 
                        t.b('        add_header X-Content-Type-Options "nosniff" always;'), t.b("\n" + i), 
                        t.b('        add_header X-Permitted-Cross-Domain-Policies "none" always;'), t.b("\n" + i), 
                        t.b('        add_header X-XSS-Protection "1; mode=block" always;'), t.b("\n"), t.b("\n" + i), 
                        t.b("        proxy_pass         http://localhost:"), t.b(t.t(t.f("port", c, p, 0))), 
                        t.b(";"), t.b("\n" + i), t.b("        proxy_http_version 1.1;"), t.b("\n" + i), 
                        t.b("        proxy_set_header Host $http_host;"), t.b("\n" + i), t.b("        proxy_set_header X-Real-IP $remote_addr;"), 
                        t.b("\n" + i), t.b("        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"), 
                        t.b("\n" + i), t.b("        proxy_set_header X-Forwarded-Proto $scheme;"), t.b("\n" + i), 
                        t.b("    }"), t.b("\n"), t.b("\n" + i), t.b("    location /ws {"), t.b("\n" + i), 
                        t.b("        proxy_read_timeout 50s;"), t.b("\n" + i), t.b("        proxy_pass         http://localhost:"), 
                        t.b(t.t(t.f("port", c, p, 0))), t.b(";"), t.b("\n" + i), t.b("        proxy_http_version 1.1;"), 
                        t.b("\n" + i), t.b("        proxy_set_header   Upgrade $http_upgrade;"), t.b("\n" + i), 
                        t.b("        proxy_set_header   Connection $connection_upgrade;"), t.b("\n" + i), 
                        t.b("    }"), t.b("\n"), t.b("\n" + i), t.b("    location /static/ {"), t.b("\n" + i), 
                        t.b('        try_files          "/$last_request_path" =404;'), t.b("\n" + i), t.b("        gzip_static        on;"), 
                        t.b("\n" + i), t.b("        gzip_proxied       any;"), t.b("\n" + i), t.b("        gzip_types         *;"), 
                        t.b("\n" + i), t.b("        access_log         off;"), t.b("\n" + i), t.b("        etag               off;"), 
                        t.b("\n" + i), t.b('        add_header         Cache-Control "public, max-age=31536000";'), 
                        t.b("\n" + i), t.b("        sendfile           on;"), t.b("\n" + i), t.b("        sendfile_max_chunk 1m;"), 
                        t.b("\n" + i), t.b("        tcp_nopush         on;"), t.b("\n" + i), t.b("    }"), 
                        t.b("\n"), t.b("\n" + i), t.b("    location /static/assets/ {"), t.b("\n" + i), 
                        t.b('        try_files          "/assets/$last_request_path" =404;'), t.b("\n" + i), 
                        t.b("        gzip_static        on;"), t.b("\n" + i), t.b("        gzip_proxied       any;"), 
                        t.b("\n" + i), t.b("        gzip_types         *;"), t.b("\n" + i), t.b("        access_log         off;"), 
                        t.b("\n" + i), t.b("        etag               off;"), t.b("\n" + i), t.b('        add_header         Cache-Control "public, max-age=31536000";'), 
                        t.b("\n" + i), t.b("        sendfile           on;"), t.b("\n" + i), t.b("        sendfile_max_chunk 1m;"), 
                        t.b("\n" + i), t.b("        tcp_nopush         on;"), t.b("\n" + i), t.b("    }"), 
                        t.b("\n" + i), t.b("}"), t.b("\n"), t.b("\n" + i);
                    })), c.pop()), t.fl();
                },
                partials: {},
                subs: {}
            });
            return T.render.apply(T, arguments);
        };
    },
    96: function(module, exports, __webpack_require__) {
        var H = __webpack_require__(21);
        module.exports = function() {
            var T = new H.Template({
                code: function(c, p, i) {
                    var t = this;
                    return t.b(i = i || ""), t.b("##"), t.b("\n" + i), t.b("## tinyproxy.conf -- tinyproxy daemon configuration file"), 
                    t.b("\n" + i), t.b("##"), t.b("\n" + i), t.b("## This example tinyproxy.conf file contains example settings"), 
                    t.b("\n" + i), t.b("## with explanations in comments. For decriptions of all"), 
                    t.b("\n" + i), t.b("## parameters, see the tinproxy.conf(5) manual page."), t.b("\n" + i), 
                    t.b("##"), t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# User/Group: This allows you to set the user and group that will be"), 
                    t.b("\n" + i), t.b("# used for tinyproxy after the initial binding to the port has been done"), 
                    t.b("\n" + i), t.b("# as the root user. Either the user or group name or the UID or GID"), 
                    t.b("\n" + i), t.b("# number may be used."), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b("User tinyproxy"), t.b("\n" + i), t.b("Group tinyproxy"), t.b("\n"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# Port: Specify the port which tinyproxy will listen on.  Please note"), 
                    t.b("\n" + i), t.b("# that should you choose to run on a port lower than 1024 you will need"), 
                    t.b("\n" + i), t.b("# to start tinyproxy using root."), t.b("\n" + i), t.b("#"), 
                    t.b("\n" + i), t.b("Port "), t.b(t.t(t.f("port", c, p, 0))), t.b("\n"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# Listen: If you have multiple interfaces this allows you to bind to"), 
                    t.b("\n" + i), t.b("# only one. If this is commented out, tinyproxy will bind to all"), 
                    t.b("\n" + i), t.b("# interfaces present."), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b("#Listen 192.168.0.1"), t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# Bind: This allows you to specify which interface will be used for"), 
                    t.b("\n" + i), t.b("# outgoing connections.  This is useful for multi-home'd machines where"), 
                    t.b("\n" + i), t.b("# you want all traffic to appear outgoing from one particular interface."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("#Bind 192.168.0.1"), t.b("\n"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# BindSame: If enabled, tinyproxy will bind the outgoing connection to the"), 
                    t.b("\n" + i), t.b("# ip address of the incoming connection."), t.b("\n" + i), t.b("#"), 
                    t.b("\n" + i), t.b("BindSame yes"), t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b("# Timeout: The maximum number of seconds of inactivity a connection is"), t.b("\n" + i), 
                    t.b("# allowed to have before it is closed by tinyproxy."), t.b("\n" + i), t.b("#"), 
                    t.b("\n" + i), t.b("Timeout 60"), t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b("# ErrorFile: Defines the HTML file to send when a given HTTP error"), t.b("\n" + i), 
                    t.b("# occurs.  You will probably need to customize the location to your"), t.b("\n" + i), 
                    t.b("# particular install.  The usual locations to check are:"), t.b("\n" + i), 
                    t.b("#   /usr/local/share/tinyproxy"), t.b("\n" + i), t.b("#   /usr/share/tinyproxy"), 
                    t.b("\n" + i), t.b("#   /etc/tinyproxy"), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b('#ErrorFile 404 "/usr/share/tinyproxy/404.html"'), t.b("\n" + i), t.b('#ErrorFile 400 "/usr/share/tinyproxy/400.html"'), 
                    t.b("\n" + i), t.b('#ErrorFile 503 "/usr/share/tinyproxy/503.html"'), t.b("\n" + i), 
                    t.b('#ErrorFile 403 "/usr/share/tinyproxy/403.html"'), t.b("\n" + i), t.b('#ErrorFile 408 "/usr/share/tinyproxy/408.html"'), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# DefaultErrorFile: The HTML file that gets sent if there is no"), 
                    t.b("\n" + i), t.b("# HTML file defined with an ErrorFile keyword for the HTTP error"), 
                    t.b("\n" + i), t.b("# that has occured."), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b('DefaultErrorFile "/usr/share/tinyproxy/default.html"'), t.b("\n"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# StatHost: This configures the host name or IP address that is treated"), 
                    t.b("\n" + i), t.b("# as the stat host: Whenever a request for this host is received,"), 
                    t.b("\n" + i), t.b("# Tinyproxy will return an internal statistics page instead of"), 
                    t.b("\n" + i), t.b("# forwarding the request to that host.  The default value of StatHost is"), 
                    t.b("\n" + i), t.b("# tinyproxy.stats."), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b('#StatHost "tinyproxy.stats"'), t.b("\n" + i), t.b("#"), t.b("\n"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# StatFile: The HTML file that gets sent when a request is made"), 
                    t.b("\n" + i), t.b("# for the stathost.  If this file doesn't exist a basic page is"), 
                    t.b("\n" + i), t.b("# hardcoded in tinyproxy."), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b('StatFile "/usr/share/tinyproxy/stats.html"'), t.b("\n"), t.b("\n" + i), t.b("#"), 
                    t.b("\n" + i), t.b("# LogFile: Allows you to specify the location where information should"), 
                    t.b("\n" + i), t.b("# be logged to.  If you would prefer to log to syslog, then disable this"), 
                    t.b("\n" + i), t.b("# and enable the Syslog directive.  These directives are mutually"), 
                    t.b("\n" + i), t.b("# exclusive. If neither Syslog nor LogFile are specified, output goes"), 
                    t.b("\n" + i), t.b("# to stdout."), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b('LogFile "/var/log/tinyproxy/tinyproxy.log"'), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# Syslog: Tell tinyproxy to use syslog instead of a logfile.  This"), 
                    t.b("\n" + i), t.b("# option must not be enabled if the Logfile directive is being used."), 
                    t.b("\n" + i), t.b("# These two directives are mutually exclusive."), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("#Syslog On"), t.b("\n"), t.b("\n" + i), t.b("#"), 
                    t.b("\n" + i), t.b("# LogLevel: Warning"), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b("# Set the logging level. Allowed settings are:"), t.b("\n" + i), t.b("#\tCritical\t(least verbose)"), 
                    t.b("\n" + i), t.b("#\tError"), t.b("\n" + i), t.b("#\tWarning"), t.b("\n" + i), 
                    t.b("#\tNotice"), t.b("\n" + i), t.b("#\tConnect\t\t(to log connections without Info's noise)"), 
                    t.b("\n" + i), t.b("#\tInfo\t\t(most verbose)"), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b("# The LogLevel logs from the set level and above. For example, if the"), t.b("\n" + i), 
                    t.b("# LogLevel was set to Warning, then all log messages from Warning to"), t.b("\n" + i), 
                    t.b("# Critical would be output, but Notice and below would be suppressed."), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("LogLevel Error"), t.b("\n"), t.b("\n" + i), t.b("#"), 
                    t.b("\n" + i), t.b("# PidFile: Write the PID of the main tinyproxy thread to this file so it"), 
                    t.b("\n" + i), t.b("# can be used for signalling purposes."), t.b("\n" + i), t.b("# If not specified, no pidfile will be written."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b('PidFile "/run/tinyproxy/tinyproxy.pid"'), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# XTinyproxy: Tell Tinyproxy to include the X-Tinyproxy header, which"), 
                    t.b("\n" + i), t.b("# contains the client's IP address."), t.b("\n" + i), t.b("#"), 
                    t.b("\n" + i), t.b("#XTinyproxy Yes"), t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b("# Upstream:"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# Turns on upstream proxy support."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# The upstream rules allow you to selectively route upstream connections"), 
                    t.b("\n" + i), t.b("# based on the host/domain of the site being accessed."), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b('# Syntax: upstream type (user:pass@)ip:port ("domain")'), 
                    t.b("\n" + i), t.b('# Or:     upstream none "domain"'), t.b("\n" + i), t.b("# The parts in parens are optional."), 
                    t.b("\n" + i), t.b("# Possible types are http, socks4, socks5, none"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# For example:"), t.b("\n" + i), t.b("#  # connection to test domain goes through testproxy"), 
                    t.b("\n" + i), t.b('#  upstream http testproxy:8008 ".test.domain.invalid"'), t.b("\n" + i), 
                    t.b('#  upstream http testproxy:8008 ".our_testbed.example.com"'), t.b("\n" + i), 
                    t.b('#  upstream http testproxy:8008 "192.168.128.0/255.255.254.0"'), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("#  # upstream proxy using basic authentication"), 
                    t.b("\n" + i), t.b('#  upstream http user:pass@testproxy:8008 ".test.domain.invalid"'), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("#  # no upstream proxy for internal websites and unqualified hosts"), 
                    t.b("\n" + i), t.b('#  upstream none ".internal.example.com"'), t.b("\n" + i), t.b('#  upstream none "www.example.com"'), 
                    t.b("\n" + i), t.b('#  upstream none "10.0.0.0/8"'), t.b("\n" + i), t.b('#  upstream none "192.168.0.0/255.255.254.0"'), 
                    t.b("\n" + i), t.b('#  upstream none "."'), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b("#  # connection to these boxes go through their DMZ firewalls"), t.b("\n" + i), 
                    t.b('#  upstream http cust1_firewall:8008 "testbed_for_cust1"'), t.b("\n" + i), 
                    t.b('#  upstream http cust2_firewall:8008 "testbed_for_cust2"'), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("#  # default upstream is internet firewall"), t.b("\n" + i), 
                    t.b("#  upstream http firewall.internal.example.com:80"), t.b("\n" + i), t.b("#"), 
                    t.b("\n" + i), t.b("# You may also use SOCKS4/SOCKS5 upstream proxies:"), t.b("\n" + i), 
                    t.b("#  upstream socks4 127.0.0.1:9050"), t.b("\n" + i), t.b("#  upstream socks5 socksproxy:1080"), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# The LAST matching rule wins the route decision.  As you can see, you"), 
                    t.b("\n" + i), t.b("# can use a host, or a domain:"), t.b("\n" + i), t.b("#  name     matches host exactly"), 
                    t.b("\n" + i), t.b('#  .name    matches any host in domain "name"'), t.b("\n" + i), 
                    t.b("#  .        matches any host with no domain (in 'empty' domain)"), t.b("\n" + i), 
                    t.b("#  IP/bits  matches network/mask"), t.b("\n" + i), t.b("#  IP/mask  matches network/mask"), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("#Upstream http some.remote.proxy:port"), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# MaxClients: This is the absolute highest number of threads which will"), 
                    t.b("\n" + i), t.b("# be created. In other words, only MaxClients number of clients can be"), 
                    t.b("\n" + i), t.b("# connected at the same time."), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b("MaxClients 100"), t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# MinSpareServers/MaxSpareServers: These settings set the upper and"), 
                    t.b("\n" + i), t.b("# lower limit for the number of spare servers which should be available."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# If the number of spare servers falls below MinSpareServers then new"), 
                    t.b("\n" + i), t.b("# server processes will be spawned.  If the number of servers exceeds"), 
                    t.b("\n" + i), t.b("# MaxSpareServers then the extras will be killed off."), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("MinSpareServers 5"), t.b("\n" + i), t.b("MaxSpareServers 20"), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# StartServers: The number of servers to start initially."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("StartServers 10"), t.b("\n"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# MaxRequestsPerChild: The number of connections a thread will handle"), 
                    t.b("\n" + i), t.b("# before it is killed. In practise this should be set to 0, which"), 
                    t.b("\n" + i), t.b("# disables thread reaping. If you do notice problems with memory"), 
                    t.b("\n" + i), t.b("# leakage, then set this to something like 10000."), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("MaxRequestsPerChild 0"), t.b("\n"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# Allow: Customization of authorization controls. If there are any"), 
                    t.b("\n" + i), t.b("# access control keywords then the default action is to DENY. Otherwise,"), 
                    t.b("\n" + i), t.b("# the default action is ALLOW."), t.b("\n" + i), t.b("#"), t.b("\n" + i), 
                    t.b("# The order of the controls are important. All incoming connections are"), 
                    t.b("\n" + i), t.b("# tested against the controls based on order."), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("Allow 127.0.0.1"), t.b("\n" + i), t.b("#Allow 192.168.0.0/16"), 
                    t.b("\n" + i), t.b("#Allow 172.16.0.0/12"), t.b("\n" + i), t.b("#Allow 10.0.0.0/8"), 
                    t.b("\n"), t.b("\n" + i), t.b('# BasicAuth: HTTP "Basic Authentication" for accessing the proxy.'), 
                    t.b("\n" + i), t.b("# If there are any entries specified, access is only granted for authenticated"), 
                    t.b("\n" + i), t.b("# users."), t.b("\n" + i), t.b("#BasicAuth user password"), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# AddHeader: Adds the specified headers to outgoing HTTP requests that"), 
                    t.b("\n" + i), t.b("# Tinyproxy makes. Note that this option will not work for HTTPS"), 
                    t.b("\n" + i), t.b("# traffic, as Tinyproxy has no control over what headers are exchanged."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b('#AddHeader "X-My-Header" "Powered by Tinyproxy"'), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b('# ViaProxyName: The "Via" header is required by the HTTP RFC, but using'), 
                    t.b("\n" + i), t.b("# the real host name is a security concern.  If the following directive"), 
                    t.b("\n" + i), t.b("# is enabled, the string supplied will be used as the host name in the"), 
                    t.b("\n" + i), t.b("# Via header; otherwise, the server's host name will be used."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b('ViaProxyName "tinyproxy"'), t.b("\n"), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# DisableViaHeader: When this is set to yes, Tinyproxy does NOT add"), 
                    t.b("\n" + i), t.b("# the Via header to the requests. This virtually puts Tinyproxy into"), 
                    t.b("\n" + i), t.b("# stealth mode. Note that RFC 2616 requires proxies to set the Via"), 
                    t.b("\n" + i), t.b("# header, so by enabling this option, you break compliance."), 
                    t.b("\n" + i), t.b("# Don't disable the Via header unless you know what you are doing..."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("DisableViaHeader Yes"), t.b("\n"), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# Filter: This allows you to specify the location of the filter file."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b('#Filter "/etc/tinyproxy/filter"'), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# FilterURLs: Filter based on URLs rather than domains."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("#FilterURLs On"), t.b("\n"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# FilterExtended: Use POSIX Extended regular expressions rather than"), 
                    t.b("\n" + i), t.b("# basic."), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("#FilterExtended On"), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# FilterCaseSensitive: Use case sensitive regular expressions."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("#FilterCaseSensitive On"), t.b("\n"), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# FilterDefaultDeny: Change the default policy of the filtering system."), 
                    t.b("\n" + i), t.b('# If this directive is commented out, or is set to "No" then the default'), 
                    t.b("\n" + i), t.b("# policy is to allow everything which is not specifically denied by the"), 
                    t.b("\n" + i), t.b("# filter file."), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b('# However, by setting this directive to "Yes" the default policy becomes'), 
                    t.b("\n" + i), t.b("# to deny everything which is _not_ specifically allowed by the filter"), 
                    t.b("\n" + i), t.b("# file."), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("#FilterDefaultDeny Yes"), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# Anonymous: If an Anonymous keyword is present, then anonymous proxying"), 
                    t.b("\n" + i), t.b("# is enabled.  The headers listed are allowed through, while all others"), 
                    t.b("\n" + i), t.b("# are denied. If no Anonymous keyword is present, then all headers are"), 
                    t.b("\n" + i), t.b("# allowed through.  You must include quotes around the headers."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# Most sites require cookies to be enabled for them to work correctly, so"), 
                    t.b("\n" + i), t.b("# you will need to allow Cookies through if you access those sites."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b('#Anonymous "Host"'), t.b("\n" + i), 
                    t.b('#Anonymous "Authorization"'), t.b("\n" + i), t.b('#Anonymous "Cookie"'), t.b("\n"), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# ConnectPort: This is a list of ports allowed by tinyproxy when the"), 
                    t.b("\n" + i), t.b("# CONNECT method is used.  To disable the CONNECT method altogether, set"), 
                    t.b("\n" + i), t.b("# the value to 0.  If no ConnectPort line is found, all ports are"), 
                    t.b("\n" + i), t.b("# allowed."), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# The following two ports are used by SSL."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("ConnectPort 443"), t.b("\n" + i), t.b("ConnectPort 563"), 
                    t.b("\n"), t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# Configure one or more ReversePath directives to enable reverse proxy"), 
                    t.b("\n" + i), t.b("# support. With reverse proxying it's possible to make a number of"), 
                    t.b("\n" + i), t.b("# sites appear as if they were part of a single site."), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# If you uncomment the following two directives and run tinyproxy"), 
                    t.b("\n" + i), t.b("# on your own computer at port 8888, you can access Google using"), 
                    t.b("\n" + i), t.b("# http://localhost:8888/google/ and Wired News using"), t.b("\n" + i), 
                    t.b("# http://localhost:8888/wired/news/. Neither will actually work"), t.b("\n" + i), 
                    t.b("# until you uncomment ReverseMagic as they use absolute linking."), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b('#ReversePath "/google/"\t"http://www.google.com/"'), 
                    t.b("\n" + i), t.b('#ReversePath "/wired/"\t"http://www.wired.com/"'), t.b("\n"), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# When using tinyproxy as a reverse proxy, it is STRONGLY recommended"), 
                    t.b("\n" + i), t.b("# that the normal proxy is turned off by uncommenting the next directive."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("#ReverseOnly Yes"), t.b("\n"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# Use a cookie to track reverse proxy mappings. If you need to reverse"), 
                    t.b("\n" + i), t.b("# proxy sites which have absolute links you must uncomment this."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("#ReverseMagic Yes"), t.b("\n"), t.b("\n" + i), 
                    t.b("#"), t.b("\n" + i), t.b("# The URL that's used to access this reverse proxy. The URL is used to"), 
                    t.b("\n" + i), t.b("# rewrite HTTP redirects so that they won't escape the proxy. If you"), 
                    t.b("\n" + i), t.b("# have a chain of reverse proxies, you'll need to put the outermost"), 
                    t.b("\n" + i), t.b("# URL here (the address which the end user types into his/her browser)."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b("# If not set then no rewriting occurs."), 
                    t.b("\n" + i), t.b("#"), t.b("\n" + i), t.b('#ReverseBaseURL "http://localhost:8888/"'), 
                    t.b("\n"), t.b("\n"), t.b("\n"), t.b("\n"), t.fl();
                },
                partials: {},
                subs: {}
            });
            return T.render.apply(T, arguments);
        };
    }
});