import { Context } from 'koa';
import { Inject, Singleton } from 'typescript-ioc';

import { EntityClass, has, RTTIItemState } from '@crud/types';
import { ClientSettings, ClientSettingsRepository } from '@entities/ClientSettings';
import bodyParseMsgpack from '@middlewares/body-parse-msgpack';
import checkEntityAccess from '@middlewares/check-entity-access';
import smartRedirect from '@middlewares/smart-redirect';
import { stringify } from '@taraflex/string-tools';
import { sendErrorEmail } from '@utils/send-email';

import { BotClient } from './BotClient';
import { ApiRouter, msgpack } from './core/ApiRouter';
import { AdminClient } from './AdminClient';
import { RPC } from '@rpc/RPC';

@Singleton
export class App {
    constructor(
        @Inject private readonly bot: BotClient,
        @Inject private readonly admin: AdminClient,
        @Inject private readonly settings: ClientSettings,
        @Inject settingsRepository: ClientSettingsRepository,
        @Inject apiRouter: ApiRouter
    ) {
        RPC.register('chats', {
            async search(query: string) {
                query = query.trim();
                if (query && admin.id) {
                    const chats = await admin.searchChats(query, 10);
                    return chats.map(c => c.title);
                } else {
                    return [];
                }
            }
        });

        apiRouter.patch('tgclient_save', '/tgclient-save/:id', checkEntityAccess(ClientSettings), bodyParseMsgpack, smartRedirect, async (ctx: Context) => {
            const body: ClientSettings = <any>ctx.request.body;

            (ClientSettings as EntityClass<ClientSettings>).insertValidate(body);

            try {
                await this.bot.update({ type: 'bot', ...body }, ctx);
            } catch (err) {
                LOG_ERROR(body, err);
                const message = stringify(err);
                throw [
                    { field: 'token', message }
                ];
            }

            try {
                await this.admin.update({ type: 'user', ...body }, ctx);
            } catch (err) {
                LOG_ERROR(body, err);
                const message = stringify(err);
                throw [
                    { field: 'phone', message }
                ];
            }

            const { props } = (ClientSettings as EntityClass).rtti;
            for (let k in body) {
                const p = props[k];
                if (p && !has(p, RTTIItemState.HIDDEN)) {
                    settings[k] = body[k];
                }
            }

            await settingsRepository.save(settings);

            ctx.status = 201;
            msgpack(ctx, settings);
        });
    }

    async init() {

        const { settings } = this;

        if (settings.phone && settings.token) {
            try {
                await this.bot.update({ type: 'bot', ...settings });
                await this.admin.update({ type: 'user', ...settings });
            } catch (err) {
                LOG_ERROR(err);
                sendErrorEmail('Ошибка запуска telegram бота');
            }
        }
    }
}