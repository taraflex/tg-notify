import escRe from 'escape-string-regexp';
import { Update } from 'tdlib-types';
import { Inject, Singleton } from 'typescript-ioc';

import { ClientSettings } from '@entities/ClientSettings';

import { BotClient } from './BotClient';
import { BaseTelegramClient } from './core/BaseTelegramClient';

const escMap = {
    '`': '`',
    '\\': '﹨',
    '\u2028': ' ',
    '\u2029': ' '
};

function esc(s: string) {
    return s ? s.replace(/`|\\|\u2028|\u2029/gu, m => escMap[m]) : '';
}

//const timeouts: Record<number, number> = Object.create(null);

@Singleton
export class AdminClient extends BaseTelegramClient {

    private keyMessageRe: RegExp = null;
    private nTpl: (key: string) => string = null;

    constructor(
        @Inject protected readonly bot: BotClient,
        @Inject protected readonly settings: ClientSettings
    ) {
        super();
        this.updateSettings();
    }

    updateSettings() {
        this.keyMessageRe = new RegExp(this.settings.keyWords.map((k, i) => `(^|[^\\p{L}])(?<_${i}>${escRe(k)})($|[^\\p{L}])`).join('|'), 'uis');
        this.nTpl = new Function('key', 'return `' + esc(this.settings.notifyMessage) + '`') as any;
    }

    protected readonly afterUpdate = async () => {
        const botId = this.bot.id;
        this.bot.adminId = this.id;
        //todo 
        LOG_INFO(botId);
        //await pMap(Array.from(await this.getChatsIds()).filter(i => i < 0 || i === botId), chat_id => this.client.invoke({ _: 'getChat', chat_id }), { concurrency: 6 });
        //await this.client.invoke({ _: 'sendBotStartMessage', bot_user_id: botId });
    }

    protected readonly onUpdate = async (update: Update) => {
        if (update._ === 'updateNewMessage') {
            const { message } = update;
            //todo
            LOG_INFO(message, this.keyMessageRe, this.nTpl);
            // let match: Record<string, string>;
            // if (
            //     !message.is_outgoing &&
            //     !message.via_bot_user_id &&
            //     message.sender_user_id &&
            //     message.content._ === 'messageText' &&
            //     (match = message.content.text.text.match(this.keyMessageRe)?.groups) &&
            //     this.settings.chats.includes((await this.client.invoke({ _: 'getChat', chat_id: message.chat_id }))?.title)
            // ) {
            //     const timedOut = !timeouts[message.sender_user_id] || Date.now() - timeouts[message.sender_user_id] > this.settings.nTimeout * 1000;

            //     try {
            //         await (timedOut || this.settings.ignoreMineTimeout) && this.settings.notifyMe && this.client.invoke({
            //             _: 'forwardMessages',
            //             chat_id: this.bot.id,
            //             from_chat_id: message.chat_id,
            //             message_ids: [message.id],
            //         });
            //     } finally {
            //         await timedOut && this.settings.notifyUser && this.send(message.sender_user_id, Object.values(match).find(Boolean));
            //     }
            // }
        }
    }
    /*
        async send(user_id: number, key: string) {
            timeouts[user_id] = Date.now();
            const chat = await this.client.invoke({ _: 'createPrivateChat', user_id: user_id });
            //todo
            LOG_INFO(chat);
            //await this.sendMarkdown(chat.id, this.nTpl(key));
        }*/
}