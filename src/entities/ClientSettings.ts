import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';
import { SingletonEntity } from '@ioc';
import { BOT_TOKEN_RE } from '@utils/tg-utils';

@Access({ GET: 0, PATCH: 0 }, { display: 'single', icon: 'cog', order: 4 }, { PATCH: 'tgclient_save' })
@SingletonEntity()
export class ClientSettings {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ min: 5, description: 'Tg телефон' })
    @Column({ default: '' })
    phone: string;

    @v({ pattern: BOT_TOKEN_RE, description: 'Токен телеграм бота, получить у [@BotFather](tg://resolve?domain=BotFather) С ботом заранее должен быть начат диалог' })
    @Column({ default: '' })
    token: string;

    @v({ description: 'Уведомлять себя' })
    @Column({ default: true })
    notifyMe: boolean;

    @v({ description: 'Игнорировать таймауты при уведомлениях себе' })
    @Column({ default: true })
    ignoreMineTimeout: Boolean;

    @v({ description: 'Уведомлять отправителя' })
    @Column({ default: true })
    notifyUser: boolean;

    @v({ type: 'md-tgclient', description: 'Markdown текст уведомления пользователю' })
    @Column({ default: 'Вы упамянули слово ${key}. [@easy_gili](tg://resolve?domain=easy_gili) это запомнил' })
    notifyMessage: string;

    @v({ remote: 'chats.search', items: { type: 'string', empty: false }, state: RTTIItemState.ALLOW_CREATE, description: 'Отслеживаемые чаты' })
    @Column({ default: '[]', type: 'simple-json' })
    chats: string[];

    @v({ items: { type: 'string', empty: false }, state: RTTIItemState.ALLOW_CREATE, description: 'Список ключевых слов' })
    @Column({ default: '[]', type: 'simple-json' })
    keyWords: string[];

    @v({ description: 'Таймаут в секундах между уведомлениями пользователю' })
    @Column({ default: 60 * 10, unsigned: true })
    nTimeout: number;
}

export abstract class ClientSettingsRepository extends Repository<ClientSettings>{ }