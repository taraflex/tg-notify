import { EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent } from 'typeorm';

@EventSubscriber()
export class ValidateSubscriber implements EntitySubscriberInterface {
    async beforeInsert({ entity }: InsertEvent<any>) {
        const f = entity.constructor.insertValidate;
        f && await f(entity);
    }
    async beforeUpdate({ entity }: UpdateEvent<any>) {
        const f = entity.constructor.updateValidate;
        f && await f(entity);
    }
}