import { EntitySubscriberInterface } from 'typeorm';

//@ts-ignore
import subscribers from 'subscribers_generated';

export default subscribers as EntitySubscriberInterface[];