import { Context } from 'koa';
import bodyParser from 'koa-body';
import { Inject, Singleton } from 'typescript-ioc';

import { EntityClass } from '@crud/types';
import { User, UserRepository } from '@entities/User';
import redirectAfterError from '@middlewares/redirect-after-error';
import { trim } from '@taraflex/string-tools';
import { get, post } from '@utils/routes-helpers';

@Singleton
export class ResetEmailRoutes {

    constructor(@Inject private readonly userRepository: UserRepository) { }

    @get({ name: 'resetemail' })
    resetemail(ctx: Context) {
        if (Date.now() - (ctx.session.confirmed || 0) < 60000 * 2) {
            ctx.pug('resetemail');
        } else {
            ctx.namedRedirect('checkpass', 0, { action: 'resetemail' });
        }
    }

    @post({ path: '/resetemail' }, redirectAfterError('resetemail'), bodyParser({
        text: false,
        json: false
    }))
    async resetemailPost(ctx: Context) {

        let { email } = ctx.request.body;
        email = trim(email);

        const user = { ...ctx.state.user, email };

        (User as EntityClass).updateValidate(user);

        await this.userRepository.save(user);
        await ctx.login(user);
        ctx.namedRedirect('root');
    }
}