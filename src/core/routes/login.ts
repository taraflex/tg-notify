import { Context } from 'koa';
import bodyParser from 'koa-body';
import passport from 'koa-passport';

import { get, post } from '@utils/routes-helpers';

export class LoginRoutes {

    @get({ name: 'login' })
    login(ctx: Context) {
        ctx.pug('login');
    }

    @post({ path: '/login' }, bodyParser({
        text: false,
        json: false
    }))
    loginPost(ctx: Context, next: () => Promise<any>) {
        return passport.authenticate('local', async (_, user, info) => {
            if (user) {
                await ctx.login(user);
                ctx.namedRedirect('root');
            } else {
                ctx.addFlash((info && info.message) || 'Invalid login data');
                ctx.namedRedirect('login');
            }
        })(ctx, next);
    }
}