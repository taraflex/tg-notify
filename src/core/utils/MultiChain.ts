export class MultiChain<T = any> {
    protected resolvers: ((value?: T | PromiseLike<T>) => void)[] = [];
    write(o: T) {
        this.resolvers.forEach(f => f(o));
        this.resolvers.length = 0;
    }
    read() {
        return new Promise<T>(resolve => this.resolvers.push(resolve));
    }
}
