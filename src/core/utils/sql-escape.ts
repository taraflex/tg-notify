export default function (s: string) {
    return JSON.stringify(s.replace(/'/g, "''"));
}