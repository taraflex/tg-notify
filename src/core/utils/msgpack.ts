import {
    createCodec, decode as dec, DecoderOptions, encode as enc, EncoderOptions
} from 'msgpack-lite';

const opts = {
    codec: createCodec({
        preset: true,
        safe: false,
        useraw: false,
        int64: false,
        binarraybuffer: true,
        uint8array: BROWSER,
        usemap: false
    })
} as EncoderOptions | DecoderOptions;
/*
const ropts = {
    codec: createCodec({
        preset: true,
        safe: false,
        useraw: false,
        int64: false,
        binarraybuffer: true,
        uint8array: true,
        usemap: false
    })
} as EncoderOptions | DecoderOptions;

Replicator.prototype.removeTransforms = function (transforms: string[]) {
    this.transforms = this.transforms.filter(t => !transforms.includes(t.type))
    for (var i = 0; i < transforms.length; i++) {
        delete this.transformsMap[transforms[i]];
    }
    return this;
};

export const replicator: {
    encode: (o: any) => Uint8Array,
    decode: (data: Uint8Array) => any
} = new Replicator({
    serialize(o: any) {
        return enc(o, ropts);
    },
    deserialize(data: Uint8Array) {
        return dec(data, ropts);
    }
});

(replicator as any).removeTransforms(['[[Date]]', '[[RegExp]]', '[[ArrayBuffer]]', '[[TypedArray]]']);
*/
export function decode(data: Uint8Array) {
    return dec(data, opts);
}

export function encode(o: any): Buffer {
    return enc(o, opts);
}