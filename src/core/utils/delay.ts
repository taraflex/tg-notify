import { CancelationToken, CancelationTokenError, IDisposable } from '@taraflex/cancelation-token';

function timeFn(connection: IDisposable, resolve: () => void) {
    connection.dispose();
    resolve();
}

export default function (time: number, cancelationToken?: CancelationToken): Promise<void> {
    return cancelationToken ? new Promise((resolve, reject) => {
        const connection = cancelationToken.connect(() => {
            clearTimeout(timeout);
            reject(new CancelationTokenError());
        });
        const timeout = setTimeout(timeFn, time, connection, resolve);
    }) : new Promise(resolve => { setTimeout(resolve, time); });
}