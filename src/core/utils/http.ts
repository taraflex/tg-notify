import got from 'got';

export { CookieJar } from 'tough-cookie';

export const USERAGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36';

export default got.extend({
    timeout: 60 * 1000,
    // cookieJar?: PromiseCookieJar | ToughCookieJar;
    ignoreInvalidCookies: true,
    //throwHttpErrors?: boolean,
    headers: {
        'Accept': '*/*',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Pragma': 'no-cache',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': USERAGENT
    },
    retry: 0,
    //localAddress?: string;
    https: {
        rejectUnauthorized: false
    }
});