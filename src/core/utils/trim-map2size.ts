export function fit2size<K, V>(m: Map<K, V>, s: number) {
    let o = m.size - s;
    if (o > 0) {
        for (let k of m.keys()) {
            m.delete(k);
            if (!--o) {
                break;
            }
        }
    }
}