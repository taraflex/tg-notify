export default function* <T>(it: T[], gSize: number) {
    if (it.length > 0) {
        if (gSize > 0 && it.length > gSize) {
            const chunksCount = Math.ceil(it.length / gSize);
            const chunkSize = Math.ceil(it.length / chunksCount);
            let g: T[] = [];
            for (let v of it) {
                if (g.length < chunkSize) {
                    g.push(v);
                } else {
                    yield g;
                    g = [v];
                }
            }
            if (g.length) {
                yield g;
            }
        } else {
            yield it;
        }
    }
}