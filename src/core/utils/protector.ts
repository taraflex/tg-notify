import base65536 from 'base65536';
import { createCipheriv, createDecipheriv } from 'crypto';
import rndBytes from 'random-bytes-seed';

import { STRING_SEED } from '@utils/rnd';

const rndGen = rndBytes(STRING_SEED);
const chachakey: Buffer = rndGen(256 / 8);
const chachaiv: Buffer = rndGen(96 / 8);
const chipterOptions = { authTagLength: 16 };
const algorithm = 'chacha20-poly1305';

export function encrypt(data: Uint8Array, r65536?: boolean): string {
    const cipher = createCipheriv(algorithm, chachakey, chachaiv, chipterOptions);
    const b = cipher.update(data);
    return r65536 ? base65536.encode(b) : b.toString('base64');
}

export function decrypt(s: string, r65536?: boolean) {
    const data = r65536 ? base65536.decode(s) : Buffer.from(s, 'base64');
    const decipher = createDecipheriv(algorithm, chachakey, chachaiv, chipterOptions);
    return decipher.update(data);
}