import koa from 'koa';
import { Inject, Singleton } from 'typescript-ioc';

import { Settings } from '@entities/Settings';

@Singleton
export class Koa extends koa {
    constructor(@Inject settings: Settings) {
        super();
        this.proxy = true;
        //load secret from persistent storage (required by koa-session)
        this.keys = [settings.secret];

        //RPC.register('ICore', this);
    }
    /*
        async eval(code: string) {
            const classNames: string[] = [];
            const instanses = [];
            ((<any>Scope.Singleton.constructor).instances as Map<Function, any>).forEach((v, k) => {
                classNames.push(k.name);
                instanses.push(v);
            });
            const asyncFn = new Function(`return async function $playground(console,require,${classNames.join(',')}){\n${code}\n}`)();
            await asyncFn(fromThis(this).remote('console'), __non_webpack_require__, ...instanses);
        }*/
}