import { Chain } from '@utils/Chain';
import { decode, encode } from '@utils/msgpack';
import rndId from '@utils/rnd-id';

type Answer = {
    //error
    e?: any,
    //result
    r?: any,
    //id
    i: number
}

type Request = {
    //class
    c: string,
    //function
    f: string,
    //arguments
    a?: any[],
    //id
    i: number
}

type ITerminal = {
    write(data: string): void;
}

class AnswerChain extends Chain<Answer> {
    readonly created = Date.now();
    constructor(readonly i: number, readonly timeout: number) {
        super();
    }
}

const CALLBACK_SOURCES = new Map<string, any>();
const CALLS = new Map<number, AnswerChain>();

const chainDropInterval = setInterval(() => {
    const now = Date.now();
    CALLS.forEach(c => {
        if (c.timeout && now - c.created > c.timeout) {
            c.write({ i: c.i, e: 'RPC call timeout' });
        }
    });
}, 10000);
if (!BROWSER) {
    chainDropInterval.unref();
}

class RemoteHandler {
    constructor(
        private readonly key: string,
        private readonly rpc: RPC,
        private readonly timeout: number
    ) { }
    get(_: any, f: string) {
        return (...a: any[]) => {
            let i = 0;
            do {
                i = rndId();
            } while (CALLS.has(i));

            const h = new AnswerChain(i, this.timeout * 1000);
            try {
                CALLS.set(i, h);
                this.rpc['send'](encode({ i, c: this.key, f, a }));//send protected
            } catch (e) {
                CALLS.delete(i);
                throw e;
            }
            return h.read().then(answer => {
                if (answer.e != null) {
                    throw answer.e;
                }
                return answer.r;
            }).finally(() => CALLS.delete(i));
        }
    }
}

const EMPTY_OBJECT = Object.freeze(Object.create(null));

export abstract class RPC {
    protected term?: ITerminal;

    static register(key: string, o: any) {
        CALLBACK_SOURCES.set(key, o);
    }

    static unregister(key: string) {
        CALLBACK_SOURCES.delete(key);
    }

    remote<I = any>(key: string, timeout?: number): { [_ in keyof I]: (...args: any[]) => Promise<any> } {
        return new Proxy(EMPTY_OBJECT, new RemoteHandler(key, this, timeout || 0));
    }

    protected async process(message: string | ArrayBuffer) {
        if (message) {
            if (message.constructor === String) {
                this.term && this.term.write(message as string);
            } else {
                const data = decode(new Uint8Array(message as ArrayBuffer, 0)) as (Request | Answer);
                if (data['f']) {
                    const { i, f, c, a } = data as Request;
                    try {
                        if (c === 'RPC') {
                            if (f === 'enableTerminal') {
                                this.send(encode({ i, r: this.enableTerminal(a[0]) }));
                            } else {
                                throw `Deny method [${f}] called on RPC`;
                            }
                        } else {
                            this.send(encode({ i, r: await this.rcall(CALLBACK_SOURCES.get(c), f, a) }));
                        }
                    } catch (e) {
                        this.send(encode({ i, e }));
                    }
                } else {
                    CALLS.get(data.i).write(data);
                    CALLS.delete(data.i);
                }
            }
        }
    }

    protected abstract rcall(o: any, fname: string, args: any[]): any;
    abstract enableTerminal(_: any): void | Promise<void>;
    protected abstract send(data: Uint8Array): void;
    abstract dispose(): void;
}