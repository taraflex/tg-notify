'use strict';

const { argv } = require('yargs');
const { basename, resolve } = require('path');
const { DefinePlugin, ContextReplacementPlugin } = require('webpack');
const { DuplicatesPlugin } = require('inspectpack/plugin');
const { IgnorePlugin } = require('webpack');
const { paramCase } = require('param-case');
const { pascalCase } = require('pascal-case');
const { sentenceCase } = require('sentence-case');
const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const fs = require('fs');
const hogan = require('hogan.js');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const nodeExternals = require('webpack-node-externals');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const VirtualModulesPlugin = require('webpack-virtual-modules');

const DEV_SERVER = basename(require.main.filename) === 'webpack-dev-server.js';
const DEV = !!(DEV_SERVER || argv.watch || (process.env.NODE_ENV && process.env.NODE_ENV.startsWith('dev')) || (argv.mode && argv.mode.toString().startsWith('dev')));

//@ts-ignore
const packageJson = require('../../package.json');
const APP_TITLE = sentenceCase(packageJson.name);
const devDependencies = Object.keys(packageJson.devDependencies);

const cssLoader = {
    loader: 'css-loader',
    options: {
        sourceMap: DEV
    }
}
const sassLoader = {
    loader: 'sass-loader',
    options: {
        sourceMap: DEV
    }
}
const postcssLoader = DEV ? undefined : {
    loader: 'postcss-loader',
    options: {
        postcssOptions: {
            plugins: [
                'postcss-flexbugs-fixes',
                'autoprefixer'
            ]
        },
        sourceMap: DEV
    }
}

const extensions = ['.js', '.ts', '.jsx', '.tsx', '.vue', '.pug'];

const u = (...a) => a.filter(Boolean);

function readDir(...parts) {
    try {
        return fs.readdirSync(resolve(...parts));
    } catch (_) { }
    return [];
}

function getEntitiesFilenames(dir, noScanCore, ext = '.ts') {
    const files = (noScanCore ? [] : readDir(__dirname, dir))
        .concat(readDir(__dirname, '../' + dir))
        .filter(n => n.endsWith(ext))
        .map(n => basename(n, ext))
        .filter(n => pascalCase(n) === n);
    return Array.from(new Set(files));
}

const nameTemplate = (ext = 'js') => `[name].${ext}?h=[contenthash:7]`;

module.exports = function config(BROWSER) {
    const BROWSER_PROD = !DEV && BROWSER;
    const styleLoader = DEV ? 'style-loader' : MiniCssExtractPlugin.loader;
    const configFile = BROWSER ? 'tsconfig_frontend.json' : 'tsconfig_node.json';
    const vueComponents = BROWSER ? getEntitiesFilenames('frontend', true, '.vue') : [];

    return {
        target: BROWSER ? 'web' : 'node',
        devtool: DEV ? (BROWSER ? 'eval' : 'eval-cheap-module-source-map') : undefined,
        mode: DEV ? 'development' : 'production',
        externals: BROWSER ? [] : [nodeExternals({ modulesFromFile: true, allowlist: devDependencies })],
        output: {
            globalObject: BROWSER ? 'self' : undefined,
            devtoolModuleFilenameTemplate: '[absolute-resource-path]',
            path: resolve(__dirname, '../../', BROWSER ? 'build/static' : 'build'),
            publicPath: BROWSER ? '/static/' : undefined,
            filename: nameTemplate(),
            chunkFilename: nameTemplate(),
            assetModuleFilename: 'assets/[name].[hash:7][ext]'
        },
        resolve: {
            extensions,
            alias: {
                'vue$': 'vue/dist/vue.runtime.esm.js',
                'typescript-ioc$': 'typescript-ioc/es6'
            },
            plugins: [new TsconfigPathsPlugin({ extensions, configFile })],
        },
        node: false,
        performance: { hints: false },
        optimization: {
            minimize: !DEV,
            minimizer: u(
                new TerserPlugin({
                    parallel: 4,
                    extractComments: false,
                    terserOptions: {
                        ecma: BROWSER_PROD ? 2017 : 2020,
                        compress: {
                            keep_classnames: true,
                            keep_fnames: true, //чтобы не сломать Function.name полифил
                            collapse_vars: false
                        },
                        output: {
                            beautify: !BROWSER,
                            comments: false
                        },
                        mangle: BROWSER_PROD ? {
                            reserved: vueComponents
                        } : false
                    }
                }),
                BROWSER_PROD ? new CssMinimizerPlugin({
                    parallel: 4,
                    test: /\.css(\?h=\w+)?$/,
                    minimizerOptions: {
                        preset: ['default', { discardComments: { removeAll: true }, zindex: false }],
                    }
                }) : 0
            ),
            splitChunks: {
                chunks: 'async',
                minChunks: 2,
                maxInitialRequests: 1000,
                maxAsyncRequests: 4
            }
        },
        plugins: u(
            DEV ? new DuplicatesPlugin() : 0,
            DEV ? new CircularDependencyPlugin({
                exclude: /node_modules/,
                allowAsyncCycles: true
            }) : 0,
            new VirtualModulesPlugin(BROWSER ? {
                'node_modules/frontend_generated.js': hogan.compile(`
{{#files}}
//@ts-ignore
import {{{.}}} from '@frontend/{{{.}}}.vue';
{{/files}}
export default [{{#files}}{{{.}}},{{/files}}];
`).render({ files: vueComponents })
            } : {
                'node_modules/entities_generated.js': hogan.compile(`
import { getMetadataArgsStorage } from 'typeorm';
import { SingletonRepository } from '@ioc';
export const entities = []; 
export const repositories = [];
const storage = getMetadataArgsStorage();
{{#files}}
const { {{{.}}}, {{{.}}}Repository } = require('@entities/{{{.}}}.ts');
if({{{.}}} && storage.filterTables({{{.}}}).length > 0) {
    entities.push({{{.}}});
    if({{{.}}}Repository) {
        repositories.push(SingletonRepository({{{.}}})({{{.}}}Repository)); 
    }
}
{{/files}}
`).render({ files: getEntitiesFilenames('entities') }),
                'node_modules/subscribers_generated.js': hogan.compile(`
{{#files}}
import { {{{.}}} } from '@subscribers/{{{.}}}';
{{/files}}
export default [{{#files}}{{{.}}},{{/files}}];
`).render({ files: getEntitiesFilenames('subscribers') }),
            }),
            new ContextReplacementPlugin(
                /moment[/\\]locale$/,
                /ru|en-gb/
            ),
            new DefinePlugin({
                BROWSER: JSON.stringify(BROWSER),
                DEBUG: JSON.stringify(DEV),
                APP_NAME: JSON.stringify(paramCase(APP_TITLE)),
                APP_TITLE: JSON.stringify(APP_TITLE),
                PROJECT_SETTINGS_EXIST: fs.existsSync(resolve(__dirname, '../entities/Settings.ts')),
                'process.env.NODE_ENV': JSON.stringify(DEV ? 'development' : 'production'),
                ...(BROWSER ? { 'process.env.DEBUG': JSON.stringify(DEV) } : {})
            }),
            BROWSER ? new IgnorePlugin({
                resourceRegExp: /^\.\/(?!javascript|typescript)[\w-]+\.js$/,
                contextRegExp: /monaco-editor[\/\\]esm[\/\\]vs[\/\\]basic-languages[\/\\]/
            }) : 0,
            BROWSER ? new IgnorePlugin({
                resourceRegExp: /^\.\/(cssMode|htmlMode|jsonMode)\.js$/,
                contextRegExp: /monaco-editor[\/\\]esm[\/\\]vs[\/\\]language[\/\\]/
            }) : 0,
            BROWSER ? new VueLoaderPlugin() : 0,
            BROWSER && fs.existsSync(resolve(__dirname, '../../favicon.ico')) ? new CopyWebpackPlugin({
                patterns: [
                    {
                        from: './favicon.ico',
                        to: './'
                    }
                ]
            }) : 0,
            BROWSER_PROD ? new MiniCssExtractPlugin({ filename: nameTemplate('css') }) : 0,
            BROWSER_PROD ? new CompressionPlugin({
                filename: '[path][name][ext].gz[query]',
                test: /\.(eot|svg|ttf|woff|js|css)(\?h=\w+)?$/,
                exclude: /common\.js(\?h=\w+)?$/,
                minRatio: 0.9
            }) : 0
        ),
        module: {
            exprContextCritical: false,
            rules: [{
                test: /\.scss$/,
                use: u(styleLoader, cssLoader, postcssLoader, sassLoader)
            }, {
                test: /\.css$/,
                use: u(styleLoader, cssLoader, postcssLoader)
            }, {
                test: /(?<!\.d)\.tsx?$/,
                use: [{
                    loader: 'ts-loader',
                    options: {
                        configFile,
                        compilerOptions: {
                            sourceMap: DEV,
                            target: BROWSER_PROD ? 'ES2017' : 'ES2020',
                            noUnusedLocals: !DEV,
                            noUnusedParameters: !DEV
                        },
                        appendTsSuffixTo: [/\.vue$/],
                    }
                }, {
                    loader: 'preprocessor-loader',
                    options: {
                        macros: {
                            LOG_ERROR: "(console.error.bind(console, '(__FILE__:__LINE__)'))",
                            LOG_WARN: "(console.warn.bind(console, '(__FILE__:__LINE__)'))",
                            LOG_INFO: "(console.log.bind(console, '(__FILE__:__LINE__)'))"
                        }
                    }
                }],
                exclude: BROWSER ? [/node_modules/] : [/frontend|node_modules/]
            }, {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        ts: 'ts-loader',
                        js: 'ts-loader'
                    },
                    esModule: true
                }
            }, {
                test: /\.(png|jpg|gif|webp|eot|svg|ttf|woff|woff2)$/,
                type: 'asset'
            }, {
                test: /\.pug$/,
                loader: 'pug-loader',
                options: {
                    pretty: DEV
                }
            }, {
                test: /\.mustache(\.conf)?$/,
                loader: 'mustache-loader',
                options: {
                    tiny: !DEV
                }
            }],
        },
        resolveLoader: {
            modules: ['node_modules', resolve(__dirname, '../loaders'), resolve(__dirname, 'loaders')]
        },
        stats: {
            hash: false,
            modules: false,
            version: false,
            children: false,
            assets: false,
            colors: true,
            depth: false,
            performance: false
        }
    }
}
module.exports.u = u;
module.exports.DEV = DEV;
module.exports.DEV_SERVER = DEV_SERVER;