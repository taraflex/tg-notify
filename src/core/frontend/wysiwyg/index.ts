import 'vue-wysiwyg/dist/vueWysiwyg.css';
import './index.scss';

import Vue from 'vue';
import w from 'vue-wysiwyg';

Vue.use(w, {
    hideModules: {
        headings: true,
        image: true,
        justifyCenter: true,
        justifyLeft: true,
        justifyRight: true,
        orderedList: true,
        separator: true,
        table: true,
        unorderedList: true,
    }
});