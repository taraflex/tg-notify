import './index.scss';

const escDiv = document.createElement('div');
escDiv.hidden = true;

function escape(html: string, mode: string) {
    escDiv.appendChild(document.createTextNode(html));
    let text = escDiv.innerHTML;
    while (escDiv.lastChild) {
        escDiv.removeChild(escDiv.lastChild);
    }
    switch (mode) {
        case 'regex': return text;
        case 'md-tgclient': return text.replace(/\*\*(.+?)\*\*/gs, '<b>**$1**</b>')
            .replace(/__(.+?)__/gs, '<i>__$1__</i>')
            .replace(/~~(.+?)~~/gs, '<span class="tgmd-strike">~~$1~~</span>')
            .replace(/```(.+?)```/gs, '<span class="tgmd-code">```$1```</span>')
            .replace(/`([^\r\n`]+?)`/g, '<span class="tgmd-code">`$1`</span>')
            .replace(/\[(.+?)\]\((.+?)\)/gs, '<span class="tgmd-link">[$1]</span><span class="tgmd-link-src">($2)</span>') + '\n';
        case 'md-tgbotlegacy': return text.replace(/\*(.+?)\*/gs, '<b>*$1*</b>')
            .replace(/_(.+?)_/gs, '<i>_$1_</i>')
            .replace(/```(.+?)```/gs, '<span class="tgmd-code">```$1```</span>')
            .replace(/`([^\r\n`]+?)`/g, '<span class="tgmd-code">`$1`</span>')
            .replace(/\[(.+?)\]\((.+?)\)/gs, '<span class="tgmd-link">[$1]</span><span class="tgmd-link-src">($2)</span>') + '\n';
    }
}

export default {
    name: 'tgmd',
    props: {
        value: String,
        mode: String,
        readonly: Boolean
    },
    data() {
        return {
            text: '',
            html: ''
        }
    },
    mounted() {
        this.text = this.value || '';
        this.html = escape(this.value || '', this.mode)
    },
    watch: {
        value(v: string) {
            this.text = v || '';
        },
        text(v: string) {
            v = v || '';
            if (v != this.value) {
                this.html = escape(v, this.mode)
                this.$emit('input', v);
            }
        }
    }
}