'use strict';

const merge = require('merge-deep');
const config = require('./webpack.base');
const { resolve } = require('path');
const AssetsPlugin = require('assets-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const jEsc = require('js-string-escape');
const fs = require('fs');
const VirtualModulesPlugin = require('webpack-virtual-modules');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const SpeedMeasurePlugin = require("speed-measure-webpack-plugin");
const { builtinModules } = require('module');
const { BannerPlugin } = require('webpack');

function getTypes() {
    //@ts-ignore
    const requires = Object.keys(require('../../package.json').dependencies).concat(builtinModules).map(m => `(id: "${m}"): typeof import("${m}");`).join('\n');

    const node = fs.readFileSync(__dirname + '/frontend/monaco/node/index.d.ts.txt', 'utf-8').replace('/* tslint:disable-next-line:callable-types */', requires);
    return fs.readFileSync(__dirname + '/../../node_modules/monaco-editor/esm/vs/language/typescript/lib/lib.js', 'utf-8')
        .replace(/\/\*! \*{2,}.+?\*{2,} \*\/\\n\\n\\n\\n\/\/\/ /g, '')
        .replace(/lib_dom_dts\s=\s".+?[^\\]"/, `lib_dom_dts = "${jEsc(node)}"`)
        .replace(/(lib_webworker_importscripts_dts|lib_scripthost_dts)\s=\s".+?[^\\]"/g, '$1 = ""');
}

if (config.DEV) {
    console.warn('DEV mode');
}

const hasVendor = !process.argv.some(s => /no-?vendor/i.test(s));
const useSmp = process.argv.some(s => /measure/i.test(s));

let smp = null;
function smp_wrap(o) {
    return useSmp ? (smp || (smp = new SpeedMeasurePlugin())).wrap(o) : o;
}

function initAssest(name, fileTypes = ['js', 'css']) {
    return new AssetsPlugin({
        fullPath: false,
        includeAllFileTypes: false,
        fileTypes,
        filename: name + '-assets.json',
        path: resolve(__dirname, '../../build'),
        entrypoints: true
    })
}

module.exports = function (webConfig, nodeConfig) {
    return config.u(
        smp_wrap(merge(merge(
            config(true),
            {
                entry: {
                    index: './src/core/frontend/index.ts'
                },
                externals: {
                    'monaco-editor': 'monaco'
                },
                module: {
                    noParse: /monaco-editor/,
                },
                plugins: config.u(
                    new BannerPlugin({
                        banner: `if(document.currentScript) {
    var src = document.currentScript['src'];
    window['LAST_SCRIPT_SRC'] = src.substring(0, src.lastIndexOf('/') + 1);
}`,
                        raw: true,
                        entryOnly: true,
                        include: /\.js/
                    }),
                    initAssest('index'),
                    !config.DEV && new BundleAnalyzerPlugin({
                        logLevel: 'warn',
                        analyzerMode: 'static',
                        openAnalyzer: false,
                        reportFilename: '../web-report.htm'
                    })
                )
            }),
            webConfig || {}
        )),
        smp_wrap(merge(merge(
            config(false),
            {
                entry: {
                    index: './src/core/index.ts'
                },
                plugins: config.u(!config.DEV && new BundleAnalyzerPlugin({
                    logLevel: 'warn',
                    analyzerMode: 'static',
                    openAnalyzer: false,
                    reportFilename: 'node-report.htm'
                }))
            }),
            nodeConfig || {}
        )),
        hasVendor && merge(
            config(true),
            {
                entry: {
                    monaco: './src/core/frontend/monaco/lib.ts'
                },
                output: {
                    library: 'monaco',
                    libraryTarget: 'window'
                },
                plugins: [
                    initAssest('monaco')
                ]
            }),
        hasVendor && merge(
            config(true),
            {
                entry: {
                    common: './src/core/frontend/common.css'
                },
                output: {
                    library: 'common'
                },
                plugins: [
                    initAssest('common', ['css']),
                    new CleanWebpackPlugin({
                        cleanStaleWebpackAssets: false,
                        cleanOnceBeforeBuildPatterns: [],
                        cleanAfterEveryBuildPatterns: ['common.js*'],
                        verbose: false
                    })
                ]
            }),
        hasVendor && merge(
            config(true),
            {
                target: 'webworker',
                entry: {
                    'editor.worker': 'monaco-editor/esm/vs/editor/editor.worker.js',
                    'typescript.worker': 'monaco-editor/esm/vs/language/typescript/ts.worker'
                },
                plugins: [
                    new VirtualModulesPlugin({
                        'node_modules/monaco-editor/esm/vs/language/typescript/lib/lib.js': getTypes()
                    }),
                    initAssest('workers'),
                ]
            })
    );
}