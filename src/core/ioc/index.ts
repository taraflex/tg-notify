import { Connection, Entity, EntityOptions } from 'typeorm';
import { Provided } from 'typescript-ioc';

import { EntityClass, RepositoryClass } from '@crud/types';
import { create, createAsync } from '@ioc/singleton-providers-factory';

export function SingletonEntity(options?: EntityOptions, disable?: boolean) {
    if (disable) {
        return function <E>(target: EntityClass<E>): EntityClass<E> {
            return target;
        }
    } else {
        const ewrap = Entity(options);
        return function <E>(target: EntityClass<E>): EntityClass<E> {
            ewrap(target);
            if (!target.asyncProvider) {
                const provider = createAsync(async (connection: Connection) => {
                    const repository = connection.getRepository(target);
                    return await repository.findOne() ||
                        await repository.createQueryBuilder().insert().values([{}]).execute() &&
                        await repository.findOneOrFail();
                });
                target.asyncProvider = provider;
                Provided(provider)(target);
            }
            return target;
        }
    }
}

export function SingletonRepository<E>(e: EntityClass<E>) {
    return function <R extends RepositoryClass<E>>(target: R) {
        if (!target.provider) {
            Provided(target.provider = create((connection: Connection) => connection.getRepository(e)))(target);
        }
        return target;
    }
}