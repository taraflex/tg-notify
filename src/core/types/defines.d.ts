declare const BROWSER: boolean;
declare const DEBUG: boolean;
declare const APP_NAME: string;
declare const APP_TITLE: string;
declare const PROJECT_SETTINGS_EXIST: boolean;
declare const v8debug: any;

declare const enum FlashLevel {
    ERROR = 0,
    WARNING = 1,
    INFO = 2,
}

declare type FlashData = Readonly<{
    messages?: string[],
    warnings?: string[],
    errors?: string[]
}>;

declare module '*.pug' {
    export default function (locals?: {
        [_: string]: any;
    }): string;
}

declare module '*.mustache' {
    export default function (locals?: {
        [_: string]: any;
    }): string;
}

declare const __non_webpack_require__: typeof require;
declare const __webpack_require__: (typeof require) & { p: string };

//browser only 
declare const EntitiesFactory: (mixins: any[], debounce: any) => any[];
declare const Paths: {
    health: string,
    resetemail: string,
    root: string,
    backup: string,
    restore: string,
    restart: string,
    logout: string,
    login: string,
    resetpass: string,
    [name: string]: string | Function
};
declare const UserInfo: { email: string, [name: string]: any };
declare const HasPty: boolean;
declare const Flash: FlashData;
declare const EDITOR_WORKER: string;
declare const TYPESCRIPT_WORKER: string;

declare const LOG_ERROR: (...args: any[]) => void;
declare const LOG_WARN: (...args: any[]) => void;
declare const LOG_INFO: (...args: any[]) => void;