import Router from 'koa-router';
import { Singleton } from 'typescript-ioc';

import catchError from '@middlewares/catch-error';
import noStore from '@middlewares/no-store';
import { HEALTH } from '@utils/config';

@Singleton
export class MainRouter extends Router {
    constructor() {
        super();
        this
            .use(catchError)
            .get('health', HEALTH, noStore, ctx => {
                ctx.type = 'json';
                ctx.body = '{"success":"ok"}';
            })
            .get('index', '/', noStore, ctx => {
                ctx.status = 200;
                ctx.body = '';
            });
    }
}