import { Context } from 'koa';

let root = '';
let rootSlash = '';

export default function (ctx: Context, next: () => Promise<any>) {
    if (ctx.method === 'GET') {
        const { path } = ctx;
        if (!root) {
            root = new URL(ctx.resolve('root')).pathname.replace(/\/$/, '');
            rootSlash = root + '/';
        }
        if (path !== '/') {
            if (root === path) {
                ctx.status = 301;
                return ctx.redirect(rootSlash + (ctx.querystring ? '?' + ctx.querystring : ''));
            } else if (path.endsWith('/') && path !== rootSlash) {
                ctx.status = 301;
                return ctx.redirect(path.slice(0, -1) + (ctx.querystring ? '?' + ctx.querystring : ''));
            }
        }
    }
    return next();
}