import { ExtendableError } from '@utils/extendable-error';

class DefferTimeout extends ExtendableError { }

export type IDeffered<T = any> = {
    createTime: number,
    promise: Promise<T>,
    fulfill: (_: T) => void,
    reject: (_: any) => void
}

export function makeDeffer<T = any>(timeout?: number) {
    const o: IDeffered<T> = {
        createTime: Date.now(),
        promise: null,
        fulfill: null,
        reject: null
    };
    o.promise = new Promise<T>((fullfil, reject) => {
        o.fulfill = fullfil;
        o.reject = reject;
    });
    if (timeout) {
        setTimeout(o.reject, timeout, new DefferTimeout()).unref();
    }
    return o;
}

export class DeferredMap<T = any>{
    protected map: Record<string | number, IDeffered<T>> = Object.create(null);
    get empty() {
        for (const _ in this.map) {
            return false;
        }
        return true;
    }
    keys() {
        return Object.keys(this.map);
    }
    createdAt(id: string | number) {
        return this.map[id].createTime;
    }
    make(id: string | number, timeout?: number) {
        return (this.map[id] ??= makeDeffer<T>(timeout)).promise;
    }
    has(id: string | number) {
        return this.map[id] != null;
    }
    fulfill(id: string | number, v: T) {
        this.map[id]?.fulfill(v);
        delete this.map[id];
    }
    reject(id: string | number, err: any) {
        this.map[id]?.reject(err);
        delete this.map[id];
    }
    rejectAll(err: any) {
        for (let id of this.keys()) {
            this.reject(id, err);
        }
    }
}