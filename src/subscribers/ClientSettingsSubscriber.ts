import { AdminClient } from 'src/AdminClient';
import { EntitySubscriberInterface, EventSubscriber, UpdateEvent } from 'typeorm';
import { Container } from 'typescript-ioc';

import { ClientSettings } from '@entities/ClientSettings';

@EventSubscriber()
export class ClientSettingsSubscriber implements EntitySubscriberInterface<ClientSettings> {

    listenTo() {
        return ClientSettings;
    }

    afterUpdate(_: UpdateEvent<ClientSettings>) {
        return (Container.get(AdminClient) as AdminClient).updateSettings();
    }
}