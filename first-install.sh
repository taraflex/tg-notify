apt-get update -y 
apt-get upgrade --yes --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages

apt-get install automake build-essential ccache clang cmake curl git gperf htop jq libc++-dev libc++abi-dev libdbus-glib-1-2 libssl-dev libx11-xcb1 libxt6 make man-db nginx openssl php-cli python software-properties-common tinyproxy tmux ufw unzip zlib1g zlib1g-dev -y

curl -fsSL https://deb.nodesource.com/setup_14.x | bash -
apt-get install nodejs -y

source ~/.bashrc

npm install -g pm2@latest
pm2 startup

curl -sSL https://git.io/get-mo -o /usr/local/bin/mo
chmod +x /usr/local/bin/mo

systemctl enable nginx
systemctl enable tinyproxy